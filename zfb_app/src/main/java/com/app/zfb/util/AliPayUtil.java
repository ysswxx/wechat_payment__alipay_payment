package com.app.zfb.util;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.response.AlipayTradeAppPayResponse;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @description: 支付宝App支付
 * @author: qiu bo yang
 * @create: 2020-03-13 09:45
 */
public class AliPayUtil {
    //服务地址
    private static final String serverUrl = "https://openapi.alipay.com/gateway.do";
    //appId
    private static final String appId = "";
    //生成私钥
    private static final String appPrivateKey = "";
    //支付宝公钥
    private static final String aliPayPublicKey = "";
    //编码
    private static final String charset = "utf-8";
    //格式
    private static final String format = "json";
    //加密类型
    private static final String signType = "RSA2";
    //产品code
    private static final String productCode = "QUICK_MSECURITY_PAY";
    //允许的最晚付款时间
    private static final String timeoutExpress = "90m";
    //异步回调地址
    private static final String notifyUrl = "http://www.dt532.cn:8091/dou_earn/app/memberProduct/zfbPayNotifyUrl";
    //描述信息
    private static final String body = "描述信息";

    /**
     * 发送订单
     *
     * @param subject     订单名称
     * @param totalAmount 订单金额
     * @param outTradeNo  订单编号
     * @return
     */
    public static String sendOrder(String subject, String totalAmount, String outTradeNo) {
        //实例化客户端
        AlipayClient alipayClient =
                new DefaultAlipayClient(serverUrl, appId, appPrivateKey, format, charset, aliPayPublicKey, signType);
        //实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
        AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
        //SDK已经封装掉了公共参数，这里只需要传入业务参数。以下方法为sdk的model入参方式(model和biz_content同时存在的情况下取biz_content)。
        AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
        model.setBody(body);
        model.setSubject(subject);
        model.setOutTradeNo(outTradeNo);
        model.setTimeoutExpress(timeoutExpress);
        model.setTotalAmount(totalAmount);
        model.setProductCode(productCode);
        request.setBizModel(model);
        request.setNotifyUrl(notifyUrl);
        String result = "";
        try {
            //这里和普通的接口调用不同，使用的是sdkExecute
            AlipayTradeAppPayResponse response = alipayClient.sdkExecute(request);
            //就是orderString 可以直接给客户端请求，无需再做处理。
            result = response.getBody();
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 支付宝验签(支付宝回调)
     *
     * @param map
     * @return
     */
    public static Boolean signCheck(Map<String, String> map) {
        try {
            return AlipaySignature.rsaCheckV1(map, aliPayPublicKey, charset, signType);
        } catch (AlipayApiException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 请求参数转化为map(支付宝回调)
     * 支付宝
     *
     * @param request (支付宝回调请求参数)
     * @return
     */
    private Map<String, String> getPayBackFormat(HttpServletRequest request) {
        Map<String, String> tempParams = new HashMap();
        Map requestParams = request.getParameterMap();
        for (Iterator it = requestParams.keySet().iterator(); it.hasNext(); ) {
            String name = (String) it.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ?
                        valueStr + values[i] : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用。
            //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            tempParams.put(name, valueStr);
        }
        return tempParams;
    }

    public static void main(String[] args) {
        String subject = "套餐一";
        String totalAmount = "0.02";
        String outTradeNo = System.currentTimeMillis() + "";
        String result = sendOrder(subject, totalAmount, outTradeNo);
        System.out.println("结果：" + result);
    }
}
