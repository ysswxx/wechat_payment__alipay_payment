package com.app.zfb.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @description: 文件工具类
 * @author: qiu bo yang
 * @create: 2020-03-14 15:26
 */
public class FileUtil {
    /**
     * 上传地址
     */
    @Value("${file.upload.address}")
    private static String uploadAddress;
    /**
     * 文件名称长度
     */
    @Value("${file.name.length}")
    private static String fileNameLength;
    /**
     * 文件大小
     */
    @Value("${file.upload.size}")
    private static String fileUploadSize;
    /**
     * 服务地址
     */
    @Value("${service.address.url}")
    private static String serviceAddressUrl;

    /**
     * 上传文件
     *
     * @param file 上传文件
     * @return
     */
    public static String uploadFile(MultipartFile file) {
        //文件路径
        String fullPath = null;
        try {
            //获取文件输入流
            InputStream in = file.getInputStream();
            //获取文件名称
            String fileName = file.getOriginalFilename();
            if (fileName.length() > Integer.valueOf(fileNameLength)) {
                throw new RuntimeException("上传文件名称过长");
            }
            //获取文件大小
            long size = file.getSize();
            if (size > Long.valueOf(fileUploadSize)) {
                throw new RuntimeException("上传文件过大");
            }
            //获取文件后缀名-类型
            String suffixType = fileName.substring(fileName.lastIndexOf("."));
            //文件上传路径
            String filePath = uploadAddress + System.currentTimeMillis() + suffixType;
            OutputStream os = new FileOutputStream(filePath);
            byte[] b = new byte[1024];
            int num;
            while ((num = in.read(b)) != -1) {
                os.write(b, 0, num);
            }
            in.close();
            os.close();
            //设置并返回全路径
            fullPath = serviceAddressUrl + filePath;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fullPath;
    }

    /**
     * 下载文件
     *
     * @param path 文件地址
     * @param response
     */
    public static void downloadFile(String path, HttpServletResponse response) {
        try {
            URL url = new URL(path);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            BufferedInputStream bis = new BufferedInputStream(con.getInputStream(), 8);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bytes = new byte[1024];
            int length = 0;
            while ((length = bis.read(bytes)) > 0) {
                byteArrayOutputStream.write(bytes, 0, length);
            }
            response.getOutputStream().write(byteArrayOutputStream.toByteArray());
            response.getOutputStream().flush();
            response.getOutputStream().close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
