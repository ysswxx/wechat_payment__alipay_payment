package com.example.zfb_website;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZfbWebsiteApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZfbWebsiteApplication.class, args);
    }

}
